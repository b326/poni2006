"""
General information from text
"""
import json
from pathlib import Path

from poni2006 import pth_clean

# read raw
info = json.load(open("../raw/info.json"))

# format
for key in tuple(info):
    if key.endswith("_comment"):
        del info[key]

# write result
info["_comment"] = f"This file has been formatted by {Path(__file__).name}"
json.dump(info, open(pth_clean / "info.json", 'w'), sort_keys=True, indent=2)
