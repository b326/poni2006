"""
Format respiration from fig7
"""
from pathlib import Path

import pandas as pd
from graphextract.svg_extractor import extract_data

from poni2006 import pth_clean

dfs = []
for cpt, suffix in [('leaf', 'a'), ('cluster', 'b'), ('stem', 'c')]:
    data = extract_data(f"../raw/fig7{suffix}.svg", x_formatter=int, y_formatter=float)
    for stage, pts in data.items():
        pts.sort()
        df = pd.DataFrame({f'{cpt}_{stage}': [v for _, v in pts],
                           't_organ': [int(t / 5 + 0.5) * 5 for t, _ in pts]}).set_index('t_organ')

        dfs.append(df)

df = pd.concat(dfs, axis='columns')

table = pd.read_csv("../raw/fig7_legend.csv", sep=";", comment="#", index_col=['organ', 'stage'])

# write resulting dataframe in a csv file
df = df[sorted(df.columns)]

with open(pth_clean / "fig7.csv", 'w', encoding='utf-8') as fhw:
    fhw.write(f"# This file has been generated by {Path(__file__).name}\n#\n")
    fhw.write("#\n")
    fhw.write("# t_organ: [°C] temperature of organ\n")
    fhw.write("# cluster_stage: [g cO2.g FW-1.h-1] Cluster respiration at given pheno stage\n")
    fhw.write("# leaf_stage: [µg cO2.m-2.h-1] Leaf respiration at given pheno stage\n")
    fhw.write("# stem_stage: [µg cO2.m-2.h-1] Stem respiration at given pheno stage\n")
    fhw.write("#\n")
    df.to_csv(fhw, sep=";", lineterminator="\n", float_format="%.3f")

with open(pth_clean / "fig7_legend.csv", 'w', encoding='utf-8') as fhw:
    fhw.write(f"# This file has been generated by {Path(__file__).name}\n#\n")
    fhw.write("#\n")
    fhw.write("# organ: [-] name of organ\n")
    fhw.write("# stage: [-] pheno stage\n")
    fhw.write("# v0: [?] value at t=0 [°C], unit depends on organ\n")
    fhw.write("# tau: [°C-1] slope of relationship\n")
    fhw.write("#\n")
    table.to_csv(fhw, sep=";", lineterminator="\n", float_format="%.4f")
