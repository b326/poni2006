"""
Figure 10
=========

Plot measures from figure 10
"""
import matplotlib.pyplot as plt
import pandas as pd

from poni2006 import pth_clean

# read meas
meas = pd.read_csv(pth_clean / "fig10.csv", sep=";", comment="#", index_col=['doy'])

# plot result
fig, axes = plt.subplots(1, 1, figsize=(8, 6), squeeze=False)
ax = axes[0, 0]
for mod in ['ld', 'hd']:
    ax.plot(meas.index, meas[f'{mod}'], 'o', label=mod)

ax.legend(loc='upper left')
ax.set_ylabel("Cumulated DM [gDM.plant-1]")
ax.set_ylim(0, 200)
ax.set_yticks(range(0, 2001, 500))

ax.set_xlabel("doy")
ax.set_xlim(90, 250)
ax.set_xticks(range(90, 251, 20))

fig.tight_layout()
plt.show()
