"""
Figure 4
========

Plot measures from figure 4
"""
import matplotlib.pyplot as plt
import pandas as pd

from poni2006 import pth_clean

# read meas
meas = pd.read_csv(pth_clean / "fig4.csv", sep=";", comment="#", index_col=['doy'])

# plot result
fig, axes = plt.subplots(1, 1, figsize=(6, 4), squeeze=False)
ax = axes[0, 0]
for mod in ['ld', 'hd']:
    ax.plot(meas.index, meas[f'{mod}'], '-o', label=mod)

ax.legend(loc='upper left')
ax.set_ylabel("Canopy light interception [%]")
ax.set_ylim(0, 50)
ax.set_yticks(range(0, 51, 5))

ax.set_xlabel("doy")
ax.set_xlim(110, 250)
ax.set_xticks(range(110, 251, 20))

fig.tight_layout()
plt.show()
