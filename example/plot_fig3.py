"""
Figure 3
========

Plot measures from figure 3
"""
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

from poni2006 import pth_clean

# read meas
meas = pd.read_csv(pth_clean / "fig3.csv", sep=";", comment="#", index_col=['nb_shoot'])

# plot equation
x_fit = np.linspace(meas.index[0], meas.index[-1], 100)
y_fit = 0.154 + 57.6 / x_fit

# plot result
fig, axes = plt.subplots(1, 1, figsize=(6, 4), squeeze=False)
ax = axes[0, 0]
ax.plot(meas.index, meas['la_growth'], 'o')
ax.plot(x_fit, y_fit, '-')

ax.set_ylabel("LA/DD [cm2.gdd-1]")
ax.set_ylim(0, 6)
ax.set_yticks(range(0, 7, 1))

ax.set_xlabel("shoots/vine [#]")
ax.set_xlim(10, 60)
ax.set_xticks(range(10, 61, 10))

fig.tight_layout()
plt.show()
