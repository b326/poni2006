"""
Figure 9
========

Plot measures from figure 9
"""
import matplotlib.pyplot as plt
import pandas as pd

from poni2006 import pth_clean

# read meas
meas = pd.read_csv(pth_clean / "fig9.csv", sep=";", comment="#", index_col=['doy'])

# plot result
fig, axes = plt.subplots(3, 1, sharex='all', figsize=(6, 8), squeeze=False)

for i, (cpt, ymax) in enumerate([('leaf', 10), ('stem', 12), ('cluster', 10)]):
    ax = axes[i, 0]
    for mod in ('hd', 'ld'):
        ax.plot(meas.index, meas[f'{cpt}_{mod}'], label=mod)

    ax.legend(loc='upper left')
    ax.set_ylabel(f"{cpt} [g CO2.vine-1.day-1]")
    ax.set_ylim(0, ymax)
    ax.set_yticks(range(0, ymax + 1))

ax = axes[-1, 0]
ax.set_xlabel("doy")
ax.set_xlim(90, 251)
ax.set_xticks(range(90, 251, 20))

fig.tight_layout()
plt.show()
