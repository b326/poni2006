"""
Figure 2
========

Plot measures from figure 2
"""
import matplotlib.pyplot as plt
import pandas as pd

from poni2006 import pth_clean

# read meas
meas = pd.read_csv(pth_clean / "fig2.csv", sep=";", comment="#", index_col=['doy'])

# plot result
fig, axes = plt.subplots(2, 1, sharex='all', sharey='all', figsize=(6, 7), squeeze=False)

for i, mod in enumerate(['ld', 'hd']):
    ax = axes[i, 0]
    ax.plot(meas.index, meas[f'{mod}_obs'], 'o')
    ax.plot(meas.index, meas[f'{mod}_sim'], '-')

    ax.set_ylabel("LA [m2]")
    ax.set_ylim(0, 6)

    ax.set_xlabel("doy")
    ax.set_xlim(90, 250)
    ax.set_xticks(range(90, 251, 20))

fig.tight_layout()
plt.show()
