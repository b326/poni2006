"""
Figure 8
========

Plot measures from figure 8
"""
import matplotlib.pyplot as plt
import pandas as pd

from poni2006 import pth_clean

# read meas
meas = pd.read_csv(pth_clean / "fig8.csv", sep=";", comment="#", index_col=['doy'])

# plot result
fig, axes = plt.subplots(1, 1, figsize=(8, 6), squeeze=False)
ax = axes[0, 0]
for mod in ['ld', 'hd']:
    ax.plot(meas.index, meas[f'{mod}_photo'], '-', label=f"{mod}_photo")
    ax.plot(meas.index, meas[f'{mod}_resp'], '--', label=f"{mod}_resp")

ax.legend(loc='upper left')
ax.set_ylabel("[µmol CO2.m-2.s-1]")
ax.set_ylim(0, 80)
ax.set_yticks(range(0, 81, 10))

ax.set_xlabel("doy")
ax.set_xlim(90, 250)
ax.set_xticks(range(90, 251, 20))

fig.tight_layout()
plt.show()
