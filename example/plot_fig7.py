"""
Figure 7
========

Plot measures from figure 7
"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from poni2006 import pth_clean

# read meas
meas = pd.read_csv(pth_clean / "fig7.csv", sep=";", comment="#", index_col=['t_organ'])
tab = pd.read_csv(pth_clean / "fig7_legend.csv", sep=";", comment="#", index_col=['organ', 'stage'])

stages = [('3w_aft_bb', '#1f77b4', 'o'),
          ('flowering', '#ff7f0e', 's'),
          ('veraison', '#2ca02c', '^'),
          ('ripening', '#d62728', 'v'),
          ('senescence', '#9467bd', 'D')]

x_fit = np.linspace(10, 35, 100)

# plot result
fig, axes = plt.subplots(3, 1, sharex='all', figsize=(6, 8), squeeze=False)

for i, (cpt, unit, ymax, ystep) in enumerate([('leaf', "µg CO2.m-2.s-1", 120, 20),
                                              ('cluster', "mg CO2.g FW-1.h-1", 2, 0.5),
                                              ('stem', "µg CO2.m-2.s-1", 1000, 200)]):
    ax = axes[i, 0]
    for stage, col, marker in stages:
        try:
            crv, = ax.plot(meas.index, meas[f'{cpt}_{stage}'], marker, color=col, label=stage)
            v0 = tab.loc[(cpt, stage), 'v0']
            tau = tab.loc[(cpt, stage), 'tau']
            ax.plot(x_fit, v0 * np.exp(tau * x_fit), '-', color=crv.get_color())
        except KeyError:
            pass

    ax.legend(loc='upper left')
    ax.set_ylabel(f"{cpt} [{unit}]")
    ax.set_ylim(0, ymax)
    ax.set_yticks([ystep * i for i in range(10) if ystep * i <= ymax])

ax = axes[-1, 0]
ax.set_xlabel("organ temperature [°C]")
ax.set_xlim(8, 37)
ax.set_xticks(range(10, 37, 5))

fig.tight_layout()
plt.show()
