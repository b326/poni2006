========================
poni2006
========================

.. {# pkglts, doc

.. image:: https://b326.gitlab.io/poni2006/_images/badge_pkging_pip.svg
    :alt: PyPI version
    :target: https://pypi.org/project/poni2006/1.2.0/

.. image:: https://b326.gitlab.io/poni2006/_images/badge_pkging_conda.svg
    :alt: Conda version
    :target: https://anaconda.org/revesansparole/poni2006

.. image:: https://b326.gitlab.io/poni2006/_images/badge_doc.svg
    :alt: Documentation status
    :target: https://b326.gitlab.io/poni2006/

.. image:: https://badge.fury.io/py/poni2006.svg
    :alt: PyPI version
    :target: https://badge.fury.io/py/poni2006

.. #}
.. {# pkglts, glabpkg_dev, after doc

main: |main_build|_ |main_coverage|_

.. |main_build| image:: https://gitlab.com/b326/poni2006/badges/main/pipeline.svg
.. _main_build: https://gitlab.com/b326/poni2006/commits/main

.. |main_coverage| image:: https://gitlab.com/b326/poni2006/badges/main/coverage.svg
.. _main_coverage: https://gitlab.com/b326/poni2006/commits/main
.. #}

Data and formalisms from Poni et al. (2006)

